init:
	docker-compose up -d
	docker-compose run --rm composer install
	@sleep 5
	docker-compose exec php bin/console doctrine:migrations:migrate -n
	@sleep 5
	docker-compose exec php bin/console prisoners:init

start:
	docker-compose up -d

down:
	docker-compose down -v

check-code:
	docker-compose exec php vendor/bin/ecs check src

fix-code:
	docker-compose exec php vendor/bin/ecs check --fix src