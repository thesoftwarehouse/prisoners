<?php declare(strict_types=1);

namespace Prisoners\Domain\Model;

final class UUID
{
    private function __construct()
    {
    }

    public static function create(): string
    {
        return \Ramsey\Uuid\Uuid::uuid4()->toString();
    }
}
