<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\User\Event;

use Broadway\Serializer\Serializable;
use Prisoners\Domain\Model\User\UserId;

final class UserWasCreatedEvent implements Serializable
{
    /**
     * @var UserId
     */
    public $id;

    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var string
     */
    public $roleName;

    public function __construct(
        UserId $id,
        string $username,
        string $password,
        string $firstName,
        string $lastName,
        string $roleName
    ) {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->roleName = $roleName;
    }

    public static function deserialize(array $data): self
    {
        return new self(
            new UserId($data['id']),
            $data['username'],
            $data['password'],
            $data['first_name'],
            $data['last_name'],
            $data['role_name']
        );
    }

    public function serialize(): array
    {
        return [
            'id' => $this->id->get(),
            'username' => $this->username,
            'password' => $this->password,
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'role_name' => $this->roleName,
        ];
    }
}
