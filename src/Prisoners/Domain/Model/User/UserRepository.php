<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\User;

interface UserRepository
{
    public function get(UserId $userId): ?User;

    public function save(User $user): void;
}
