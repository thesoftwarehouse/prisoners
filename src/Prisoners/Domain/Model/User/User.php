<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\User;

use Prisoners\Domain\Model\AggregateRoot;
use Prisoners\Domain\Model\User\Event\UserWasCreatedEvent;
use Prisoners\Domain\Model\User\Event\UserWasDestroyedEvent;

final class User extends AggregateRoot
{
    /**
     * @var UserId
     */
    private $id;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var Role
     */
    private $role;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var bool
     */
    private $destroyed;

    protected function __construct()
    {
        $this->destroyed = false;
    }

    public static function create(
        UserId $userId,
        string $username,
        string $password,
        string $firstName,
        string $lastName,
        Role $role
    ): self {
        $user = new static();
        $user->apply(
            new UserWasCreatedEvent($userId, $username, $password, $firstName, $lastName, $role->getName())
        );

        return $user;
    }

    public function getAggregateRootId(): string
    {
        return $this->id->get();
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getRole(): Role
    {
        return $this->role;
    }

    public function destroy(): void
    {
        $this->apply(
            new UserWasDestroyedEvent($this->id)
        );
    }

    public function isActive(): bool
    {
        return ! $this->destroyed;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    protected function applyUserWasCreatedEvent(UserWasCreatedEvent $event): void
    {
        $this->id = $event->id;
        $this->firstName = $event->firstName;
        $this->lastName = $event->lastName;
        $this->role = new Role($event->roleName);
    }

    protected function applyUserWasDestroyedEvent(UserWasDestroyedEvent $event): void
    {
        $this->destroyed = true;
    }
}
