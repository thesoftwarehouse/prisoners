<?php declare(strict_types=1);

namespace Prisoners\Domain\Model;

use Broadway\EventSourcing\EventSourcedAggregateRoot;

abstract class AggregateRoot extends EventSourcedAggregateRoot
{
    public static function instantiateForReconstitution(): self
    {
        return new static();
    }
}
