<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\Penitentiary\Event;

use Broadway\Serializer\Serializable;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;
use Prisoners\Domain\Model\Prisoner\PrisonerId;

final class PrisonerWasReleasedEvent implements Serializable
{
    /**
     * @var PenitentiaryId
     */
    public $penitentiaryId;

    /**
     * @var PrisonerId
     */
    public $prisonerId;

    public function __construct(PenitentiaryId $penitentiaryId, PrisonerId $prisonerId)
    {
        $this->penitentiaryId = $penitentiaryId;
        $this->prisonerId = $prisonerId;
    }

    public static function deserialize(array $data): self
    {
        return new static(
            new PenitentiaryId($data['penitentiary_id']),
            new PrisonerId($data['prisoner_id'])
        );
    }

    public function serialize(): array
    {
        return [
            'penitentiary_id' => $this->penitentiaryId->get(),
            'prisoner_id' => $this->prisonerId->get(),
        ];
    }
}
