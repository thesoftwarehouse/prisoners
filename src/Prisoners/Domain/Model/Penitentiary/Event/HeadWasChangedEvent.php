<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\Penitentiary\Event;

use Broadway\Serializer\Serializable;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;
use Prisoners\Domain\Model\User\UserId;

final class HeadWasChangedEvent implements Serializable
{
    /**
     * @var PenitentiaryId
     */
    public $penitentiaryId;

    /**
     * @var UserId
     */
    public $headId;

    public function __construct(PenitentiaryId $penitentiaryId, UserId $headId)
    {
        $this->penitentiaryId = $penitentiaryId;
        $this->headId = $headId;
    }

    public static function deserialize(array $data): self
    {
        return new static(
            new PenitentiaryId($data['penitentiary_id']),
            new UserId($data['head_id'])
        );
    }

    public function serialize(): array
    {
        return [
            'penitentiary_id' => $this->penitentiaryId->get(),
            'head_id' => $this->headId->get(),
        ];
    }
}
