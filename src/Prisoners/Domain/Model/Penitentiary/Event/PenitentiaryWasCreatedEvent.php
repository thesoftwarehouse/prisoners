<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\Penitentiary\Event;

use Broadway\Serializer\Serializable;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;

final class PenitentiaryWasCreatedEvent implements Serializable
{
    /**
     * @var PenitentiaryId
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * PenitentiaryWasCreatedEvent constructor.
     */
    public function __construct(PenitentiaryId $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public static function deserialize(array $data): self
    {
        return new static(new PenitentiaryId($data['id']), $data['name']);
    }

    public function serialize(): array
    {
        return [
            'id' => $this->id->get(),
            'name' => $this->name,
        ];
    }
}
