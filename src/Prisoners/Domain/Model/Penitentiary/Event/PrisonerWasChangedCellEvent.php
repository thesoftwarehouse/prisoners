<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\Penitentiary\Event;

use Broadway\Serializer\Serializable;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;
use Prisoners\Domain\Model\Prisoner\PrisonerId;

final class PrisonerWasChangedCellEvent implements Serializable
{
    /**
     * @var PenitentiaryId
     */
    public $penitentiaryId;

    /**
     * @var PrisonerId
     */
    public $prisonerId;

    /**
     * @var int
     */
    public $cellNumber;

    /**
     * @var string
     */
    public $block;

    public function __construct(PenitentiaryId $penitentiaryId, PrisonerId $prisonerId, string $block, int $cellNumber)
    {
        $this->penitentiaryId = $penitentiaryId;
        $this->prisonerId = $prisonerId;
        $this->block = $block;
        $this->cellNumber = $cellNumber;
    }

    public static function deserialize(array $data): self
    {
        return new static(
            new PenitentiaryId($data['penitentiary_id']),
            new PrisonerId($data['prisoner_id']),
            $data['block'],
            $data['cell_number']
        );
    }

    public function serialize(): array
    {
        return [
            'penitentiary_id' => $this->penitentiaryId->get(),
            'prisoner_id' => $this->prisonerId->get(),
            'block' => $this->block,
            'cell_number' => $this->cellNumber,
        ];
    }
}
