<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\Penitentiary;

final class PenitentiaryId
{
    /**
     * @var string
     */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function get(): string
    {
        return $this->id;
    }
}
