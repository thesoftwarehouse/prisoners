<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\Penitentiary;

use Prisoners\Domain\Model\AggregateRoot;
use Prisoners\Domain\Model\Penitentiary\Event\HeadWasChangedEvent;
use Prisoners\Domain\Model\Penitentiary\Event\PenitentiaryWasCreatedEvent;
use Prisoners\Domain\Model\Penitentiary\Event\PrisonerWasAssignedEvent;
use Prisoners\Domain\Model\Penitentiary\Event\PrisonerWasChangedCellEvent;
use Prisoners\Domain\Model\Penitentiary\Event\PrisonerWasReleasedEvent;
use Prisoners\Domain\Model\Prisoner\PrisonerId;
use Prisoners\Domain\Model\User\UserId;

final class Penitentiary extends AggregateRoot
{
    /**
     * @var PenitentiaryId
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var PrisonerCell[]
     */
    private $prisonerCells;

    /**
     * @var UserId
     */
    private $headId;

    protected function __construct()
    {
        $this->prisonerCells = [];
    }

    public static function create(PenitentiaryId $id, string $name): self
    {
        $penitentiary = new static();
        $penitentiary->apply(
            new PenitentiaryWasCreatedEvent($id, $name)
        );

        return $penitentiary;
    }

    public function getId(): PenitentiaryId
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAggregateRootId(): string
    {
        return $this->getId()->get();
    }

    public function assignPrisoner(PrisonerId $prisonerId, string $block, int $cellNumber): void
    {
        $this->apply(new PrisonerWasAssignedEvent($this->id, $prisonerId, $block, $cellNumber));
    }

    public function changeCell(PrisonerId $prisonerId, string $block, int $cellNumber): void
    {
        $this->apply(new PrisonerWasChangedCellEvent($this->id, $prisonerId, $block, $cellNumber));
    }

    public function releasePrisoner(PrisonerId $prisonerId): void
    {
        $this->apply(new PrisonerWasReleasedEvent($this->id, $prisonerId));
    }

    public function setHead(UserId $userId): void
    {
        $this->apply(new HeadWasChangedEvent($this->id, $userId));
    }

    public function getHeadId(): UserId
    {
        return $this->headId;
    }

    /**
     * @return PrisonerCell[]
     */
    public function getPrisonerCells(): array
    {
        return $this->prisonerCells;
    }

    protected function applyPenitentiaryWasCreatedEvent(PenitentiaryWasCreatedEvent $event): void
    {
        $this->id = $event->id;
        $this->name = $event->name;
    }

    protected function applyPrisonerWasAssignedEvent(PrisonerWasAssignedEvent $event): void
    {
        $pc = new PrisonerCell();
        $pc->prisonerId = $event->prisonerId;
        $pc->block = $event->block;
        $pc->number = $event->cellNumber;

        $this->prisonerCells[] = $pc;
    }

    protected function applyPrisonerWasChangedCellEvent(PrisonerWasChangedCellEvent $event): void
    {
        foreach ($this->prisonerCells as $index => $prisonerCell) {
            if ($prisonerCell->prisonerId->get() === $event->prisonerId->get()) {
                $this->prisonerCells[$index]->block = $event->block;
                $this->prisonerCells[$index]->cell = $event->cellNumber;
                return;
            }
        }

        throw new \Exception(sprintf('Prisoner %s is not assigned', $event->prisonerId->get()));
    }

    protected function applyPrisonerWasReleasedEvent(PrisonerWasReleasedEvent $event): void
    {
        foreach ($this->prisonerCells as $index => $prisonerCell) {
            if ($prisonerCell->prisonerId->get() === $event->prisonerId->get()) {
                unset($this->prisonerCells[$index]);
                return;
            }
        }

        throw new \Exception(sprintf('Prisoner %s is not assigned', $event->prisonerId->get()));
    }

    protected function applyHeadWasChangedEvent(HeadWasChangedEvent $event): void
    {
        $this->headId = $event->headId;
    }
}
