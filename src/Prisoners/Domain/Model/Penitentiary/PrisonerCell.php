<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\Penitentiary;

use Prisoners\Domain\Model\Prisoner\PrisonerId;

final class PrisonerCell
{
    /**
     * @var int
     */
    public $number;

    /**
     * @var string
     */
    public $block;

    /**
     * @var PrisonerId
     */
    public $prisonerId;
}
