<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\Prisoner\Event;

use Broadway\Serializer\Serializable;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;
use Prisoners\Domain\Model\Prisoner\PrisonerId;

final class PrisonerWasAddedEvent implements Serializable
{
    /**
     * @var PrisonerId
     */
    public $id;

    /**
     * @var string
     */
    public $firstName;

    /**
     * @var string
     */
    public $lastName;

    /**
     * @var PenitentiaryId
     */
    public $penitentiaryId;

    /**
     * @var string
     */
    public $crime;

    /**
     * @var string
     */
    public $crimeCategory;

    /**
     * @var \DateTimeInterface
     */
    public $startDate;

    /**
     * @var \DateTimeInterface
     */
    public $endDate;

    /**
     * @var string
     */
    public $notes;

    /**
     * PrisonerWasAddedEvent constructor.
     */
    public function __construct(
        PrisonerId $id,
        string $firstName,
        string $lastName,
        string $crime,
        string $crimeCategoryId,
        \DateTimeInterface $startDate,
        \DateTimeInterface $endDate,
        string $notes
    ) {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->crime = $crime;
        $this->crimeCategory = $crimeCategoryId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->notes = $notes;
    }

    public static function deserialize(array $data): self
    {
        return new self(
            new PrisonerId($data['id']),
            $data['first_name'],
            $data['last_name'],
            $data['crime'],
            $data['crime_category_id'],
            new \DateTimeImmutable($data['start_date']),
            new \DateTimeImmutable($data['end_date']),
            $data['notes']
        );
    }

    public function serialize(): array
    {
        return [
            'id' => $this->id->get(),
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'crime' => $this->crime,
            'crime_category_id' => $this->crimeCategory,
            'start_date' => $this->startDate->format(DATE_RFC3339_EXTENDED),
            'end_date' => $this->endDate->format(DATE_RFC3339_EXTENDED),
            'notes' => $this->notes,
        ];
    }
}
