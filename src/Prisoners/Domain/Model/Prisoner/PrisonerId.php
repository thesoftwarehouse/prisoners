<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\Prisoner;

final class PrisonerId
{
    /**
     * @var string
     */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function get(): string
    {
        return $this->id;
    }
}
