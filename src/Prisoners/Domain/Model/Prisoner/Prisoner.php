<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\Prisoner;

use Prisoners\Domain\Model\AggregateRoot;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;
use Prisoners\Domain\Model\Person;
use Prisoners\Domain\Model\Prisoner\Crime\CrimeCategory;
use Prisoners\Domain\Model\Prisoner\Event\PrisonerWasAddedEvent;

final class Prisoner extends AggregateRoot
{
    /**
     * @var PrisonerId
     */
    private $prisonerId;

    /**
     * @var Person
     */
    private $person;

    /**
     * @var PenitentiaryId
     */
    private $penitentiaryId;

    /**
     * @var Crime
     */
    private $crime;

    /**
     * @var DurationOfStay
     */
    private $durationOfStay;

    /**
     * @var string
     */
    private $notes;

    protected function __construct()
    {
    }

    /**
     * @param string|null $notes
     * @return Prisoner
     */
    public static function create(
        PrisonerId $prisonerId,
        Person $person,
        Crime $crime,
        DurationOfStay $durationOfStay,
        ?string $notes
    ): self {
        $prisoner = new static();

        $prisoner->apply(
            new PrisonerWasAddedEvent(
                $prisonerId,
                $person->getFirstName(),
                $person->getLastName(),
                $crime->getDescription(),
                $crime->getCategory()->getName(),
                $durationOfStay->getStartDate(),
                $durationOfStay->getEndDate(),
                $notes
            )
        );

        return $prisoner;
    }

    public function getAggregateRootId(): string
    {
        return $this->prisonerId->get();
    }

    public function getPrisonerId(): PrisonerId
    {
        return $this->prisonerId;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function getPenitentiaryId(): PenitentiaryId
    {
        return $this->penitentiaryId;
    }

    public function getCrime(): Crime
    {
        return $this->crime;
    }

    public function getDurationOfStay(): DurationOfStay
    {
        return $this->durationOfStay;
    }

    public function getNotes(): string
    {
        return $this->notes;
    }

    public function setNotes(string $notes): void
    {
        $this->notes = $notes;
    }

    protected function applyPrisonerWasAddedEvent(PrisonerWasAddedEvent $event): void
    {
        $this->prisonerId = $event->id;
        $this->person = new Person($event->firstName, $event->lastName);
        $this->penitentiaryId = $event->penitentiaryId;
        $this->crime = new Crime($event->crime, new CrimeCategory($event->crimeCategory));
        $this->durationOfStay = new DurationOfStay($event->startDate, $event->endDate);
        $this->notes = $event->notes;
    }
}
