<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\Prisoner;

use Prisoners\Domain\Model\Prisoner\Crime\CrimeCategory;

final class Crime
{
    /**
     * @var string
     */
    private $description;

    /**
     * @var CrimeCategory
     */
    private $category;

    /**
     * Crime constructor.
     */
    public function __construct(string $description, CrimeCategory $category)
    {
        $this->description = $description;
        $this->category = $category;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getCategory(): CrimeCategory
    {
        return $this->category;
    }
}
