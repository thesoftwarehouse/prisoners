<?php declare(strict_types=1);

namespace Prisoners\Domain\Model\Prisoner\Crime;

final class CrimeCategory
{
    /**
     * @var string
     */
    private $name;

    /**
     * CrimeCategory constructor.
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
