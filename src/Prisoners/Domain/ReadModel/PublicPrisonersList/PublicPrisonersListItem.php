<?php declare(strict_types=1);

namespace Prisoners\Domain\ReadModel\PublicPrisonersList;

use Broadway\ReadModel\SerializableReadModel;

final class PublicPrisonersListItem implements SerializableReadModel
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $crimeCategory;

    /**
     * @var \DateTimeInterface
     */
    public $startDate;

    /**
     * @var \DateTimeInterface
     */
    public $endDate;

    public function getId(): string
    {
        return $this->id;
    }

    public static function deserialize(array $data): self
    {
        $item = new static();
        $item->name = $data['name'];
        $item->crimeCategory = $data['crime_category'];
        $item->startDate = new \DateTimeImmutable($data['start_date']);
        $item->endDate = new \DateTimeImmutable($data['end_date']);
        $item->id = $data['id'];

        return $item;
    }

    public function serialize(): array
    {
        return [
            'name' => $this->name,
            'crime_category' => $this->crimeCategory,
            'start_date' => $this->startDate->format(DATE_RFC3339_EXTENDED),
            'end_date' => $this->endDate->format(DATE_RFC3339_EXTENDED),
            'id' => $this->id,
        ];
    }
}
