<?php declare(strict_types=1);

namespace Prisoners\Domain\ReadModel\Prosecutor;

use Broadway\ReadModel\Projector;
use Prisoners\Domain\Model\Penitentiary\Event\PrisonerWasAssignedEvent;
use Prisoners\Domain\Model\Penitentiary\Event\PrisonerWasReleasedEvent;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryRepository;
use Prisoners\Domain\Model\Prisoner\PrisonerRepository;

final class ProsecutorPrisonersListProjector extends Projector
{
    /**
     * @var ProsecutorPrisonersListRepository
     */
    private $prosecutorPrisonersListRepository;

    /**
     * @var PenitentiaryRepository
     */
    private $penitentiaryRepository;

    /**
     * @var PrisonerRepository
     */
    private $prisonerRepository;

    public function __construct(
        ProsecutorPrisonersListRepository $prosecutorPrisonersListRepository,
        PenitentiaryRepository $penitentiaryRepository,
        PrisonerRepository $prisonerRepository
    ) {
        $this->prosecutorPrisonersListRepository = $prosecutorPrisonersListRepository;
        $this->penitentiaryRepository = $penitentiaryRepository;
        $this->prisonerRepository = $prisonerRepository;
    }

    protected function applyPrisonerWasAssignedEvent(PrisonerWasAssignedEvent $prisonerWasAssignedEvent): void
    {
        $penitentiary = $this->penitentiaryRepository->get($prisonerWasAssignedEvent->penitentiaryId);
        $prisoner = $this->prisonerRepository->get($prisonerWasAssignedEvent->prisonerId);

        $prisonerReadModel = new ProsecutorPrisoner(
            $prisonerWasAssignedEvent->prisonerId->get(),
            sprintf('%s %s', $prisoner->getPerson()->getLastName(), $prisoner->getPerson()->getFirstName()),
            $prisonerWasAssignedEvent->penitentiaryId->get(),
            $penitentiary->getName(),
            $prisoner->getCrime()->getDescription(),
            $prisoner->getDurationOfStay()->getStartDate(),
            $prisoner->getDurationOfStay()->getEndDate()
        );

        $this->prosecutorPrisonersListRepository->save($prisonerReadModel);
    }

    protected function applyPrisonerWasReleasedEvent(PrisonerWasReleasedEvent $prisonerWasReleasedEvent): void
    {
        $this->prosecutorPrisonersListRepository->delete($prisonerWasReleasedEvent->prisonerId);
    }
}
