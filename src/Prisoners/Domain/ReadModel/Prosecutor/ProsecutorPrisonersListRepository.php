<?php declare(strict_types=1);

namespace Prisoners\Domain\ReadModel\Prosecutor;

use Prisoners\Domain\Model\Prisoner\PrisonerId;

interface ProsecutorPrisonersListRepository
{
    public function fetchAll(): array;

    public function save(ProsecutorPrisoner $item): void;

    public function delete(PrisonerId $prisonerId): void;
}
