<?php declare(strict_types=1);

namespace Prisoners\Domain\ReadModel\Prosecutor;

use Broadway\ReadModel\SerializableReadModel;

final class ProsecutorPrisoner implements SerializableReadModel
{
    /**zz
     * @var string
     */
    public $prisonerId;

    /**
     * @var string
     */
    public $prisonerName;

    /**
     * @var string
     */
    public $penitentiaryId;

    /**
     * @var string
     */
    public $penitentiaryName;

    /**
     * @var \DateTimeInterface
     */
    public $from;

    /**
     * @var \DateTimeInterface
     */
    public $to;

    /**
     * @var string
     */
    public $crime;

    public function __construct(
        string $prisonerId,
        string $prisonerName,
        string $penitentiaryId,
        string $penitentiaryName,
        string $crime,
        \DateTimeInterface $from,
        \DateTimeInterface $to
    ) {
        $this->prisonerId = $prisonerId;
        $this->prisonerName = $prisonerName;
        $this->penitentiaryId = $penitentiaryId;
        $this->penitentiaryName = $penitentiaryName;
        $this->crime = $crime;
        $this->from = $from;
        $this->to = $to;
    }

    public function getId(): string
    {
        return $this->prisonerId;
    }

    public static function deserialize(array $data): self
    {
        return new static(
            $data['prisoner_id'],
            $data['prisoner_name'],
            $data['penitentiary_id'],
            $data['penitentiary_name'],
            $data['crime'],
            new \DateTimeImmutable($data['from']),
            new \DateTimeImmutable($data['to'])
        );
    }

    public function serialize(): array
    {
        return [
            'prisoner_id' => $this->prisonerId,
            'prisoner_name' => $this->prisonerName,
            'penitentiary_id' => $this->penitentiaryId,
            'penitentiary_name' => $this->penitentiaryName,
            'crime' => $this->crime,
            'from' => $this->from->format(DATE_RFC3339_EXTENDED),
            'to' => $this->to->format(DATE_RFC3339_EXTENDED),
        ];
    }
}
