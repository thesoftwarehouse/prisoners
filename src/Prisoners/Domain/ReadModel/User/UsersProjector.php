<?php declare(strict_types=1);

namespace Prisoners\Domain\ReadModel\User;

use Broadway\ReadModel\Projector;
use Prisoners\Domain\Model\User\Event\UserWasCreatedEvent;

final class UsersProjector extends Projector
{
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    public function __construct(UsersRepository $usersRepository)
    {
        $this->usersRepository = $usersRepository;
    }

    protected function applyUserWasCreatedEvent(UserWasCreatedEvent $userWasCreatedEvent): void
    {
        $user = new User($userWasCreatedEvent->username, $userWasCreatedEvent->password, [$userWasCreatedEvent->roleName]);

        $this->usersRepository->save($user);
    }
}
