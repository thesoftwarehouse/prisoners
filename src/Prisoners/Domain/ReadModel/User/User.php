<?php declare(strict_types=1);

namespace Prisoners\Domain\ReadModel\User;

use Broadway\ReadModel\SerializableReadModel;
use Symfony\Component\Security\Core\User\UserInterface;

final class User implements UserInterface, SerializableReadModel
{
    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string[]
     */
    private $roles;

    /**
     * @var string
     */
    private $headOf;

    /**
     * @param string[] $roles
     */
    public function __construct(
        string $username,
        string $password,
        array $roles
    ) {
        $this->username = $username;
        $this->password = $password;
        $this->roles = $roles;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getSalt(): void
    {
        // TODO: Implement getSalt() method.
    }

    public function eraseCredentials(): void
    {
        // TODO: Implement eraseCredentials() method.
    }

    public static function deserialize(array $data): self
    {
        $user = new static($data['username'], $data['password'], $data['roles']);
        $user->firstName = $data['first_name'];
        $user->lastName = $data['last_name'];
        $user->headOf = $data['head_of'];

        return $user;
    }

    public function serialize(): array
    {
        return [
            'username' => $this->username,
            'password' => $this->password,
            'first_name' => $this->firstName,
            'last_name' => $this->lastName,
            'roles' => $this->roles,
            'head_of' => $this->headOf,
        ];
    }

    public function getId(): string
    {
        return $this->username;
    }

    public function setAsHeadOf(string $penitentiaryId): void
    {
        $this->headOf = $penitentiaryId;
    }

    public function refuseHeadOf(): void
    {
        $this->headOf = null;
    }

    public function getHeadOf(): ?string
    {
        return $this->headOf;
    }

    public function isAdmin(): bool
    {
        return \in_array('ROLE_ADMIN', $this->roles, true);
    }
}
