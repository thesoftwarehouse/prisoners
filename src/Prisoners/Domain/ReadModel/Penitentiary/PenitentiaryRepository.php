<?php declare(strict_types=1);

namespace Prisoners\Domain\ReadModel\Penitentiary;

interface PenitentiaryRepository
{
    public function fetchAll(): array;

    public function get(string $penitentiaryId): Penitentiary;

    public function save(Penitentiary $penitentiary): void;

    public function delete(string $penitentiaryId): void;
}
