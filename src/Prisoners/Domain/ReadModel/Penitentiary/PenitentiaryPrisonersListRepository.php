<?php declare(strict_types=1);

namespace Prisoners\Domain\ReadModel\Penitentiary;

use Prisoners\Domain\Model\Prisoner\PrisonerId;

interface PenitentiaryPrisonersListRepository
{
    public function fetchAll(): array;

    public function findForPenitentiary(string $penitentiaryId): array;

    public function save(Prisoner $item): void;

    public function delete(PrisonerId $prisonerId): void;
}
