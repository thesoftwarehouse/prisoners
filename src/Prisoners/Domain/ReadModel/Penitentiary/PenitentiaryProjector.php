<?php declare(strict_types=1);

namespace Prisoners\Domain\ReadModel\Penitentiary;

use Broadway\ReadModel\Projector;
use Prisoners\Domain\Model\Penitentiary\Event\HeadWasChangedEvent;
use Prisoners\Domain\Model\Penitentiary\Event\PenitentiaryWasCreatedEvent;
use Prisoners\Domain\ReadModel\User\UsersRepository;

final class PenitentiaryProjector extends Projector
{
    /**
     * @var \Prisoners\Domain\ReadModel\Penitentiary\PenitentiaryRepository
     */
    private $penitentiaryReadModelRepository;

    /**
     * @var UsersRepository
     */
    private $usersRepository;

    public function __construct(
        PenitentiaryRepository $penitentiaryReadModelRepository,
        UsersRepository $usersRepository
    ) {
        $this->penitentiaryReadModelRepository = $penitentiaryReadModelRepository;
        $this->usersRepository = $usersRepository;
    }

    protected function applyPenitentiaryWasCreatedEvent(PenitentiaryWasCreatedEvent $penitentiaryWasCreatedEvent): void
    {
        $penitentiaryReadModel = new Penitentiary(
            $penitentiaryWasCreatedEvent->id->get(),
            $penitentiaryWasCreatedEvent->name
        );

        $this->penitentiaryReadModelRepository->save($penitentiaryReadModel);
    }

    protected function applyHeadWasChangedEvent(HeadWasChangedEvent $headWasChangedEvent): void
    {
        $penitentiaryReadModel = $this->penitentiaryReadModelRepository->get($headWasChangedEvent->penitentiaryId->get());
        $oldHeadId = $penitentiaryReadModel->getHeadId();
        if ($oldHeadId !== null) {
            $oldHead = $this->usersRepository->find($oldHeadId);
            $oldHead->refuseHeadOf();
            $this->usersRepository->save($oldHead);
        }
        $newHead = $this->usersRepository->find($headWasChangedEvent->headId->get());

        $penitentiaryReadModel->setHeadId($headWasChangedEvent->headId->get());
        $newHead->setAsHeadOf($penitentiaryReadModel->getId());

        $this->penitentiaryReadModelRepository->save($penitentiaryReadModel);
        $this->usersRepository->save($newHead);
    }
}
