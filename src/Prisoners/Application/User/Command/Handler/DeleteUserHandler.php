<?php declare(strict_types=1);

namespace Prisoners\Application\User\Command\Handler;

use Broadway\CommandHandling\SimpleCommandHandler;
use Prisoners\Application\User\Command\DeleteUserCommand;
use Prisoners\Domain\Model\User\User;
use Prisoners\Domain\Model\User\UserRepository;

final class DeleteUserHandler extends SimpleCommandHandler
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function handleDeleteUserCommand(DeleteUserCommand $removeUserCommand): void
    {
        /** @var User$user */
        $user = $this->userRepository->get($removeUserCommand->userId);

        if ($user === null) {
            throw new \Exception('User does not exist');
        }

        $user->destroy();
        $this->userRepository->save($user);
    }
}
