<?php declare(strict_types=1);

namespace Prisoners\Application\User\Command\Handler;

use Broadway\CommandHandling\SimpleCommandHandler;
use Prisoners\Application\User\Command\CreateUserCommand;
use Prisoners\Domain\Model\User\Role;
use Prisoners\Domain\Model\User\User;
use Prisoners\Domain\Model\User\UserRepository;

final class CreateUserHandler extends SimpleCommandHandler
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function handleCreateUserCommand(CreateUserCommand $createUserCommand): void
    {
        $user = User::create(
            $createUserCommand->userId,
            $createUserCommand->username,
            $createUserCommand->password,
            $createUserCommand->firstName,
            $createUserCommand->lastName,
            new Role($createUserCommand->roleName)
        );

        $this->userRepository->save($user);
    }
}
