<?php declare(strict_types=1);

namespace Prisoners\Application\Penitentiary\Command;

use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;
use Prisoners\Domain\Model\Prisoner\PrisonerId;

final class ReleasePrisonerCommand
{
    /**
     * @var PenitentiaryId
     */
    public $penitentiaryId;

    /**
     * @var PrisonerId
     */
    public $prisonerId;

    public function __construct(PenitentiaryId $penitentiaryId, PrisonerId $prisonerId)
    {
        $this->penitentiaryId = $penitentiaryId;
        $this->prisonerId = $prisonerId;
    }
}
