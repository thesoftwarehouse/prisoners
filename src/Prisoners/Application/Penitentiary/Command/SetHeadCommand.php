<?php declare(strict_types=1);

namespace Prisoners\Application\Penitentiary\Command;

use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;
use Prisoners\Domain\Model\User\UserId;

final class SetHeadCommand
{
    /**
     * @var PenitentiaryId
     */
    public $penitentiaryId;

    /**
     * @var UserId
     */
    public $userId;

    public function __construct(PenitentiaryId $penitentiaryId, UserId $userId)
    {
        $this->penitentiaryId = $penitentiaryId;
        $this->userId = $userId;
    }
}
