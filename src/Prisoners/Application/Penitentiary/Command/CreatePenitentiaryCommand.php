<?php declare(strict_types=1);

namespace Prisoners\Application\Penitentiary\Command;

use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;

final class CreatePenitentiaryCommand
{
    /**
     * @var PenitentiaryId
     */
    public $penitentiaryId;

    /**
     * @var string
     */
    public $name;
}
