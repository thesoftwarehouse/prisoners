<?php declare(strict_types=1);

namespace Prisoners\Application\Penitentiary\Command;

use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;
use Prisoners\Domain\Model\Prisoner\PrisonerId;

final class ChangeCellCommand
{
    /**
     * @var PenitentiaryId
     */
    public $penitentiaryId;

    /**
     * @var PrisonerId
     */
    public $prisonerId;

    /**
     * @var string
     */
    public $block;

    /**
     * @var integer
     */
    public $cellNumber;

    /**
     * AssignPrisonerCommand constructor.
     */
    public function __construct(PenitentiaryId $penitentiaryId, PrisonerId $prisonerId, string $block, int $cellNumber)
    {
        $this->penitentiaryId = $penitentiaryId;
        $this->prisonerId = $prisonerId;
        $this->block = $block;
        $this->cellNumber = $cellNumber;
    }
}
