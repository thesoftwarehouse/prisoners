<?php declare(strict_types=1);

namespace Prisoners\Application\Penitentiary\Command\Handler;

use Broadway\CommandHandling\SimpleCommandHandler;
use Prisoners\Application\Penitentiary\Command\ReleasePrisonerCommand;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryRepository;

final class ReleasePrisonerHandler extends SimpleCommandHandler
{
    /**
     * @var PenitentiaryRepository
     */
    private $penitentiaryRepository;

    public function __construct(PenitentiaryRepository $penitentiaryRepository)
    {
        $this->penitentiaryRepository = $penitentiaryRepository;
    }

    public function handleReleasePrisonerCommand(ReleasePrisonerCommand $releasePrisonerCommand): void
    {
        $penitentiary = $this->penitentiaryRepository->get($releasePrisonerCommand->penitentiaryId);

        if ($penitentiary === null) {
            throw new \Exception('Penitentiary not found');
        }

        $penitentiary->releasePrisoner($releasePrisonerCommand->prisonerId);

        $this->penitentiaryRepository->save($penitentiary);
    }
}
