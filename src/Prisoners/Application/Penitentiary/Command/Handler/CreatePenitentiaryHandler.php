<?php declare(strict_types=1);

namespace Prisoners\Application\Penitentiary\Command\Handler;

use Broadway\CommandHandling\SimpleCommandHandler;
use Prisoners\Application\Penitentiary\Command\CreatePenitentiaryCommand;
use Prisoners\Domain\Model\Penitentiary\Penitentiary;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryRepository;

final class CreatePenitentiaryHandler extends SimpleCommandHandler
{
    /**
     * @var PenitentiaryRepository
     */
    private $penitentiaryRepository;

    public function __construct(PenitentiaryRepository $penitentiaryRepository)
    {
        $this->penitentiaryRepository = $penitentiaryRepository;
    }

    public function handleCreatePenitentiaryCommand(CreatePenitentiaryCommand $createPenitentiaryCommand): void
    {
        $penitentiary = Penitentiary::create(
            $createPenitentiaryCommand->penitentiaryId,
            $createPenitentiaryCommand->name
        );

        $this->penitentiaryRepository->save($penitentiary);
    }
}
