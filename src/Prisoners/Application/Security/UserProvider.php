<?php declare(strict_types=1);

namespace Prisoners\Application\Security;

use Prisoners\Domain\ReadModel\User\UsersRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

final class UserProvider implements UserProviderInterface
{
    /**
     * @var UsersRepository
     */
    private $userRepository;

    public function __construct(UsersRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $username
     */
    public function loadUserByUsername($username): UserInterface
    {
        return $this->fetchUser($username);
    }

    /**
     * @param string $username
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        $username = $user->getUsername();

        return $this->fetchUser($username);
    }

    public function supportsClass($class): bool
    {
        return $class === UserInterface::class;
    }

    private function fetchUser(string $username): ?UserInterface
    {
        return $this->userRepository->findByUsername($username);
    }
}
