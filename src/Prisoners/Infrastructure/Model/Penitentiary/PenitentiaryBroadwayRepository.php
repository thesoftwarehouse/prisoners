<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\Model\Penitentiary;

use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\NamedConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;
use Prisoners\Domain\Model\Penitentiary\Penitentiary;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryRepository;

final class PenitentiaryBroadwayRepository implements PenitentiaryRepository
{
    /**
     * @var EventSourcingRepository
     */
    private $eventSourcingRepository;

    public function __construct(
        EventStore $eventStore,
        EventBus $eventBus,
        array $eventStreamDecorators = []
    ) {
        $this->eventSourcingRepository = new EventSourcingRepository(
            $eventStore,
            $eventBus,
            Penitentiary::class,
            new NamedConstructorAggregateFactory(),
            $eventStreamDecorators
        );
    }

    public function get(PenitentiaryId $penitentiaryId): ?Penitentiary
    {
        return $this->eventSourcingRepository->load($penitentiaryId->get());
    }

    public function save(Penitentiary $user): void
    {
        $this->eventSourcingRepository->save($user);
    }
}
