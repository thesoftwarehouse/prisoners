<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\ReadModel;

use Broadway\ReadModel\ElasticSearch\ElasticSearchRepositoryFactory;
use Broadway\ReadModel\Repository;

abstract class AbstractRepository
{
    /**
     * @var ElasticSearchRepositoryFactory
     */
    private $repositoryFactory;

    /**
     * @var ElasticSearchRepositoryFactory
     */
    private $repository;

    /**
     * AbstractRepository constructor.
     */
    public function __construct(ElasticSearchRepositoryFactory $repositoryFactory)
    {
        $this->repositoryFactory = $repositoryFactory;
    }

    protected function getRepository(): Repository
    {
        if ($this->repository === null) {
            $this->repository = $this->repositoryFactory->create($this->getName(), $this->getClass());
        }

        return $this->repository;
    }

    abstract protected function getName(): string;

    abstract protected function getClass(): string;
}
