<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\ReadModel\PublicPrisonersList;

use Prisoners\Domain\Model\Prisoner\PrisonerId;
use Prisoners\Domain\ReadModel\PublicPrisonersList\PublicPrisonersListItem;
use Prisoners\Domain\ReadModel\PublicPrisonersList\PublicPrisonersListRepository;
use Prisoners\Infrastructure\ReadModel\AbstractRepository;

final class PublicPrisonersListBroadwayRepository extends AbstractRepository implements PublicPrisonersListRepository
{
    public function fetchAll(): array
    {
        return $this->getRepository()->findAll();
    }

    public function save(PublicPrisonersListItem $item): void
    {
        $this->getRepository()->save($item);
    }

    public function delete(PrisonerId $prisonerId): void
    {
        $this->getRepository()->remove($prisonerId->get());
    }

    protected function getName(): string
    {
        return 'public_prisoners_repository';
    }

    protected function getClass(): string
    {
        return PublicPrisonersListItem::class;
    }
}
