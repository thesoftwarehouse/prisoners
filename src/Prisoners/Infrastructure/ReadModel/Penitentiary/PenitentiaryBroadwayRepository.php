<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\ReadModel\Penitentiary;

use Prisoners\Domain\ReadModel\Penitentiary\Penitentiary;
use Prisoners\Domain\ReadModel\Penitentiary\PenitentiaryRepository;
use Prisoners\Infrastructure\ReadModel\AbstractRepository;

final class PenitentiaryBroadwayRepository extends AbstractRepository implements PenitentiaryRepository
{
    public function fetchAll(): array
    {
        return $this->getRepository()->findAll();
    }

    public function findForPenitentiary(string $penitentiaryId): array
    {
        return $this->getRepository()->findBy([
            'penitentiary_id' => $penitentiaryId,
        ]);
    }

    public function save(Penitentiary $penitentiary): void
    {
        $this->getRepository()->save($penitentiary);
    }

    public function delete(string $penitentiaryId): void
    {
        $this->getRepository()->remove($penitentiaryId);
    }

    public function get(string $penitentiaryId): Penitentiary
    {
        return $this->getRepository()->find($penitentiaryId);
    }

    protected function getName(): string
    {
        return 'penitentiary_repository';
    }

    protected function getClass(): string
    {
        return Penitentiary::class;
    }
}
