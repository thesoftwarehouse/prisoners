<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\ReadModel\Penitentiary;

use Prisoners\Domain\Model\Prisoner\PrisonerId;
use Prisoners\Domain\ReadModel\Penitentiary\PenitentiaryPrisonersListRepository;
use Prisoners\Domain\ReadModel\Penitentiary\Prisoner;
use Prisoners\Infrastructure\ReadModel\AbstractRepository;

final class PenitentiaryPrisonersListBroadwayRepository extends AbstractRepository implements PenitentiaryPrisonersListRepository
{
    public function fetchAll(): array
    {
        return $this->getRepository()->findAll();
    }

    public function findForPenitentiary(string $penitentiaryId): array
    {
        return $this->getRepository()->findBy([
            'penitentiary_id' => $penitentiaryId,
        ]);
    }

    public function save(Prisoner $prisoner): void
    {
        $this->getRepository()->save($prisoner);
    }

    public function delete(PrisonerId $prisonerId): void
    {
        $this->getRepository()->remove($prisonerId->get());
    }

    protected function getName(): string
    {
        return 'penitentiary_prisoners_repository';
    }

    protected function getClass(): string
    {
        return Prisoner::class;
    }
}
