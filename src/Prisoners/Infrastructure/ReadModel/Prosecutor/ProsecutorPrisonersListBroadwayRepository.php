<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\ReadModel\Prosecutor;

use Prisoners\Domain\Model\Prisoner\PrisonerId;
use Prisoners\Domain\ReadModel\Prosecutor\ProsecutorPrisoner;
use Prisoners\Domain\ReadModel\Prosecutor\ProsecutorPrisonersListRepository;
use Prisoners\Infrastructure\ReadModel\AbstractRepository;

final class ProsecutorPrisonersListBroadwayRepository extends AbstractRepository implements ProsecutorPrisonersListRepository
{
    public function fetchAll(): array
    {
        return $this->getRepository()->findAll();
    }

    public function save(ProsecutorPrisoner $prisoner): void
    {
        $this->getRepository()->save($prisoner);
    }

    public function delete(PrisonerId $prisonerId): void
    {
        $this->getRepository()->remove($prisonerId->get());
    }

    protected function getName(): string
    {
        return 'prosecutor_prisoners_repository';
    }

    protected function getClass(): string
    {
        return ProsecutorPrisoner::class;
    }
}
