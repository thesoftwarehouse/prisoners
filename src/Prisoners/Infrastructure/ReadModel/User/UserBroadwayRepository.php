<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\ReadModel\User;

use Prisoners\Domain\ReadModel\User\User;
use Prisoners\Domain\ReadModel\User\UsersRepository;
use Prisoners\Infrastructure\ReadModel\AbstractRepository;
use Symfony\Component\Security\Core\User\UserInterface;

final class UserBroadwayRepository extends AbstractRepository implements UsersRepository
{
    public function save(UserInterface $user): void
    {
        $this->getRepository()->save($user);
    }

    public function find(string $userId): User
    {
        return $this->getRepository()->find($userId);
    }

    public function findByUsername(string $username): User
    {
        return current($this->getRepository()->findBy(['username' => $username]));
    }

    protected function getName(): string
    {
        return 'user_repository';
    }

    protected function getClass(): string
    {
        return User::class;
    }
}
