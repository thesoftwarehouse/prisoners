<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\Command;

use Broadway\CommandHandling\CommandBus;
use Prisoners\Application\Penitentiary\Command\ChangeCellCommand;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;
use Prisoners\Domain\Model\Prisoner\PrisonerId;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class ChangeCellCliCommand extends Command
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();
        $this->commandBus = $commandBus;
    }

    protected function configure(): void
    {
        $this->setName('prisoners:change-cell');
        $this->addArgument('penitentiaryId', InputArgument::REQUIRED, 'Penitentiary id');
        $this->addArgument('prisonerId', InputArgument::REQUIRED, 'Prisoner id');
        $this->addArgument('block', InputArgument::REQUIRED, 'Block');
        $this->addArgument('cellNumber', InputArgument::REQUIRED, 'Cell number');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $penitentiaryId = $input->getArgument('penitentiaryId');
        $prisonerId = $input->getArgument('prisonerId');
        $block = $input->getArgument('block');
        $cellNumber = $input->getArgument('cellNumber');

        $assignPrisoner = new ChangeCellCommand(new PenitentiaryId($penitentiaryId), new PrisonerId($prisonerId), $block, (int) $cellNumber);

        $this->commandBus->dispatch($assignPrisoner);

        $output->writeln('<info>Prisoner successfully moved</info>');
    }
}
