<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\Command;

use Broadway\CommandHandling\CommandBus;
use Prisoners\Application\Prisoner\Command\AddPrisonerCommand;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;
use Prisoners\Domain\Model\Prisoner\PrisonerId;
use Prisoners\Domain\Model\UUID;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

final class AddPrisonerCliCommand extends Command
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();
        $this->commandBus = $commandBus;
    }

    protected function configure(): void
    {
        $this->setName('prisoners:add-prisoner');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $helper = $this->getHelper('question');

        $firstNameQuestion = new Question('First name: ');
        $lastNameQuestion = new Question('Last name: ');
        $penitentiaryIdQuestion = new ChoiceQuestion(
            'Penitentiary id: ',
            array_values($this->getPenitentiaries())
        );

        $crimeCategoryQuestion = new ChoiceQuestion('Crime category: ', [
            'none',
        ]);

        $crimeQuestion = new Question('Crime: ');
        $notesQuestion = new Question('Notes: ');

        $firstName = $helper->ask($input, $output, $firstNameQuestion);
        $lastName = $helper->ask($input, $output, $lastNameQuestion);
        $penitentiaryName = $helper->ask($input, $output, $penitentiaryIdQuestion);
        $crimeCategory = $helper->ask($input, $output, $crimeCategoryQuestion);
        $crime = $helper->ask($input, $output, $crimeQuestion);
        $notes = $helper->ask($input, $output, $notesQuestion);

        $penitentiaryId = $this->resolvePenitentiaryId($penitentiaryName);

        $addPrisonerCommand = new AddPrisonerCommand();
        $addPrisonerCommand->prisonerId = new PrisonerId(UUID::create());
        $addPrisonerCommand->firstName = $firstName;
        $addPrisonerCommand->lastName = $lastName;
        $addPrisonerCommand->crimeCategory = $crimeCategory;
        $addPrisonerCommand->crime = $crime;
        $addPrisonerCommand->startDate = new \DateTimeImmutable();
        $addPrisonerCommand->endDate = new \DateTimeImmutable('+1 month');
        $addPrisonerCommand->notes = $notes ?? '';
        $addPrisonerCommand->penitentiaryId = new PenitentiaryId($penitentiaryId);

        $this->commandBus->dispatch($addPrisonerCommand);

        $output->writeln('<info>Prisoner successfully added</info>');
    }

    private function getPenitentiaries(): array
    {
        return [
            'b43f3cce-cc6c-40dd-b161-aaba4d1d4ed4' => 'ZK Wojkowice',
        ];
    }

    private function resolvePenitentiaryId(string $penitentiaryName): string
    {
        $penitentiaries = $this->getPenitentiaries();

        return array_search($penitentiaryName, $penitentiaries, true);
    }
}
