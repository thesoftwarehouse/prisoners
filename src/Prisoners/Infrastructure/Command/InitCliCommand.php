<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\Command;

use Broadway\CommandHandling\CommandBus;
use Prisoners\Application\Penitentiary\Command\AssignPrisonerCommand;
use Prisoners\Application\Penitentiary\Command\CreatePenitentiaryCommand;
use Prisoners\Application\Prisoner\Command\AddPrisonerCommand;
use Prisoners\Application\User\Command\CreateUserCommand;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;
use Prisoners\Domain\Model\Prisoner\PrisonerId;
use Prisoners\Domain\Model\User\UserId;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class InitCliCommand extends Command
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();
        $this->commandBus = $commandBus;
    }

    protected function configure(): void
    {
        $this->setName('prisoners:init');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $createUserCommand = new CreateUserCommand();
        $createUserCommand->userId = new UserId('admin');
        $createUserCommand->firstName = 'Admin';
        $createUserCommand->lastName = 'Admin';
        $createUserCommand->roleName = 'ROLE_ADMIN';
        $createUserCommand->username = 'admin';
        $createUserCommand->password = 'admin';

        $this->commandBus->dispatch($createUserCommand);

        $createUserCommand = new CreateUserCommand();
        $createUserCommand->userId = new UserId('sue');
        $createUserCommand->firstName = 'Sue';
        $createUserCommand->lastName = 'Doe';
        $createUserCommand->roleName = 'ROLE_HEAD';
        $createUserCommand->username = 'sue';
        $createUserCommand->password = 'sue123';

        $this->commandBus->dispatch($createUserCommand);

        $createPenitentiaryCommand = new CreatePenitentiaryCommand();
        $createPenitentiaryCommand->penitentiaryId = new PenitentiaryId('zkwojkowice');
        $createPenitentiaryCommand->name = 'ZK Wojkowice';

        $this->commandBus->dispatch($createPenitentiaryCommand);

        $createPenitentiaryCommand = new CreatePenitentiaryCommand();
        $createPenitentiaryCommand->penitentiaryId = new PenitentiaryId('zkwronki');
        $createPenitentiaryCommand->name = 'ZK Wronki';

        $this->commandBus->dispatch($createPenitentiaryCommand);

        $output->writeln('<info>Penitentiary successfully created</info>');

        $addPrisonerCommand = new AddPrisonerCommand();
        $addPrisonerCommand->prisonerId = new PrisonerId('donaldduck');
        $addPrisonerCommand->firstName = 'Donald';
        $addPrisonerCommand->lastName = 'Duck';
        $addPrisonerCommand->crimeCategory = 'Kradzież';
        $addPrisonerCommand->crime = 'Kradzież lizaków';
        $addPrisonerCommand->startDate = new \DateTimeImmutable();
        $addPrisonerCommand->endDate = new \DateTimeImmutable('+5 years +1 month');
        $addPrisonerCommand->notes = 'Agresywny';

        $this->commandBus->dispatch($addPrisonerCommand);

        $addPrisonerCommand = new AddPrisonerCommand();
        $addPrisonerCommand->prisonerId = new PrisonerId('pinkpanther');
        $addPrisonerCommand->firstName = 'Pink';
        $addPrisonerCommand->lastName = 'Panther';
        $addPrisonerCommand->crimeCategory = 'Kradzież';
        $addPrisonerCommand->crime = 'Kradzież cukierków';
        $addPrisonerCommand->startDate = new \DateTimeImmutable();
        $addPrisonerCommand->endDate = new \DateTimeImmutable('+18 month');
        $addPrisonerCommand->notes = 'Spokojny';

        $this->commandBus->dispatch($addPrisonerCommand);

        $addPrisonerCommand = new AddPrisonerCommand();
        $addPrisonerCommand->prisonerId = new PrisonerId('daffyduck');
        $addPrisonerCommand->firstName = 'Daffy';
        $addPrisonerCommand->lastName = 'Duck';
        $addPrisonerCommand->crimeCategory = 'Kradzież';
        $addPrisonerCommand->crime = 'Kradzież cukierków';
        $addPrisonerCommand->startDate = new \DateTimeImmutable();
        $addPrisonerCommand->endDate = new \DateTimeImmutable('+24 month');
        $addPrisonerCommand->notes = 'Spokojny';

        $this->commandBus->dispatch($addPrisonerCommand);

        $output->writeln('<info>Prisoners successfully added</info>');

        $assignPrisoner = new AssignPrisonerCommand(new PenitentiaryId('zkwojkowice'), new PrisonerId('donaldduck'), 'B', 13);
        $this->commandBus->dispatch($assignPrisoner);

        $assignPrisoner = new AssignPrisonerCommand(new PenitentiaryId('zkwojkowice'), new PrisonerId('pinkpanther'), 'C', 7);
        $this->commandBus->dispatch($assignPrisoner);

        $assignPrisoner = new AssignPrisonerCommand(new PenitentiaryId('zkwronki'), new PrisonerId('daffyduck'), 'C', 8);
        $this->commandBus->dispatch($assignPrisoner);

        $output->writeln('<info>Prisoners successfully assigned</info>');
    }
}
