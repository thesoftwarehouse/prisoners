<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\Command;

use Broadway\CommandHandling\CommandBus;
use Prisoners\Application\Penitentiary\Command\CreatePenitentiaryCommand;
use Prisoners\Domain\Model\Penitentiary\PenitentiaryId;
use Prisoners\Domain\Model\UUID;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

final class CreatePenitentiaryCliCommand extends Command
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();
        $this->commandBus = $commandBus;
    }

    protected function configure(): void
    {
        $this->setName('prisoners:create-penitentiary');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $helper = $this->getHelper('question');

        $nameQuestion = new Question('Name: ');

        $name = $helper->ask($input, $output, $nameQuestion);

        $createPenitentiaryCommand = new CreatePenitentiaryCommand();
        $createPenitentiaryCommand->penitentiaryId = new PenitentiaryId(UUID::create());
        $createPenitentiaryCommand->name = $name;

        $this->commandBus->dispatch($createPenitentiaryCommand);
    }
}
