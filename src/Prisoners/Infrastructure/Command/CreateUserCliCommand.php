<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\Command;

use Broadway\CommandHandling\CommandBus;
use Prisoners\Application\User\Command\CreateUserCommand;
use Prisoners\Domain\Model\User\UserId;
use Prisoners\Domain\Model\UUID;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

final class CreateUserCliCommand extends Command
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        parent::__construct();
        $this->commandBus = $commandBus;
    }

    protected function configure(): void
    {
        $this->setName('prisoners:create-user');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $helper = $this->getHelper('question');

        $firstNameQuestion = new Question('First name: ');
        $lastNameQuestion = new Question('Last name: ');
        $usernameQuestion = new Question('Username: ');
        $passwordQuestion = new Question('Password: ');
        $roleQuestion = new ChoiceQuestion('Role: ', ['ROLE_HEAD', 'ROLE_PROSECUTOR', 'ROLE_ADMIN']);

        $firstName = $helper->ask($input, $output, $firstNameQuestion);
        $lastName = $helper->ask($input, $output, $lastNameQuestion);
        $username = $helper->ask($input, $output, $usernameQuestion);
        $password = $helper->ask($input, $output, $passwordQuestion);
        $role = $helper->ask($input, $output, $roleQuestion);

        $createUserCommand = new CreateUserCommand();
        $createUserCommand->userId = new UserId(UUID::create());
        $createUserCommand->firstName = $firstName;
        $createUserCommand->lastName = $lastName;
        $createUserCommand->roleName = $role;
        $createUserCommand->username = $username;
        $createUserCommand->password = $password;

        $this->commandBus->dispatch($createUserCommand);
    }
}
