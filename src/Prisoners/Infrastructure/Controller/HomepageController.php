<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\Controller;

use Prisoners\Domain\ReadModel\PublicPrisonersList\PublicPrisonersListRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

final class HomepageController extends Controller
{
    public function public(PublicPrisonersListRepository $prisonersListRepository): Response
    {
        $prisoners = $prisonersListRepository->fetchAll();

        return $this->render('public-list.html.twig', ['prisoners' => $prisoners]);
    }

    public function index(): Response
    {
        return $this->redirect('/public');
    }
}
