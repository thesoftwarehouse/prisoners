<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\Controller;

use Prisoners\Domain\ReadModel\Prosecutor\ProsecutorPrisonersListRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

final class ProsecutorPrisonersController extends Controller
{
    public function index(ProsecutorPrisonersListRepository $prosecutorPrisonersListRepository): Response
    {
        $prisoners = $prosecutorPrisonersListRepository->fetchAll();

        return $this->render('prosecutor-prisoners.html.twig', ['prisoners' => $prisoners]);
    }
}
