<?php declare(strict_types=1);

namespace Prisoners\Infrastructure\Controller;

use Prisoners\Domain\ReadModel\Penitentiary\PenitentiaryPrisonersListRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

final class PenitentiaryPrisonersController extends Controller
{
    public function index(PenitentiaryPrisonersListRepository $penitentiaryPrisonersListRepository): Response
    {
        $user = $this->getUser();

        if ($user->isAdmin()) {
            $prisoners = $penitentiaryPrisonersListRepository->fetchAll();
        } else {
            $prisoners = $penitentiaryPrisonersListRepository->findForPenitentiary($user->getHeadOf());
        }

        return $this->render('my-prisoners.html.twig', ['prisoners' => $prisoners]);
    }
}
