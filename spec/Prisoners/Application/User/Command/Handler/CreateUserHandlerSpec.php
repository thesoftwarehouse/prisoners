<?php

namespace spec\Prisoners\Application\User\Command\Handler;

use Prisoners\Domain\Model\User\UserId;
use Prisoners\Domain\Model\User\UserRepository;
use Prisoners\Application\User\Command\CreateUserCommand;
use PhpSpec\ObjectBehavior;
use Prisoners\Domain\Model\User\User;
use Prisoners\Domain\Model\UUID;
use Prophecy\Argument;

class CreateUserHandlerSpec extends ObjectBehavior
{
    public function it_handles_create_user_command(UserRepository $userRepository): void
    {
        $this->beConstructedWith($userRepository);
        $userRepository->save(Argument::type(User::class))->shouldBeCalled();

        $command = new CreateUserCommand();
        $command->userId = new UserId(UUID::create());
        $command->username = 'johnny.bravo';
        $command->password = 'jb123';
        $command->firstName = 'Johnny';
        $command->lastName = 'Bravo';
        $command->roleName = 'prosecutor';

        $this->handle($command);
    }
}
