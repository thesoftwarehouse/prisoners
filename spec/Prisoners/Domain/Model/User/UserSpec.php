<?php

namespace spec\Prisoners\Domain\Model\User;

use Prisoners\Domain\Model\User\UserId;
use Prisoners\Domain\Model\User\Role;
use Prisoners\Domain\Model\User\User;
use PhpSpec\ObjectBehavior;
use Prisoners\Domain\Model\UUID;

class UserSpec extends ObjectBehavior
{
    public function it_creates_a_user(): void
    {
        $prosecutor = new Role('prosecutor');
        $this->beConstructedThrough('create', [new UserId(UUID::create()),'johnny.bravo', 'jb123', 'Johnny', 'Bravo', $prosecutor]);
        $this->shouldBeAnInstanceOf(User::class);
        $this->getAggregateRootId()->shouldBeString();
    }
}
