#!/bin/sh

apt-get update && apt-get install -y unzip
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
composer install
vendor/bin/ecs check src
vendor/bin/phpspec run
