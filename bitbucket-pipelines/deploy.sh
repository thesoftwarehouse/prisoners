#!/bin/sh

ssh -tt deploy@68.183.71.250 "
    cd prisoners
    sed -i -e '/^DATA_TAG/ d' .env && echo 'DATA_TAG=$BITBUCKET_COMMIT' >> .env;
    docker-compose build php
    docker-compose up --no-deps -d php
"